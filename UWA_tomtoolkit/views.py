from django.http import HttpResponse
from django.views import View
from tom_targets.models import Target


class GraceDB(View):
    def get(self, request, *args, **kwargs):
        if request.headers.get('content-type') != 'application/json':
            return HttpResponse("Request content must be json.")

        json = request.json()
        targets = None

        # TODO: Should unprocessed events get processed here.
        if 'gracedb_id' in json:
            targets = Target.objects.all().filter(
                name__startswith=f"coinc-{json['gracedb_id']}"
            )
        elif 'start_unixtime' in json and 'end_unixtime' in json:
            targets = Target.objects.all().filter(
                epoch__gte=json['start_unixtime'],
                epoch__lte=json['end_unixtime'],
                name__startswith="coinc-",
            )

        # TODO: what format do the alerts we send need to be?
        # TODO: How to include event artifacts like skymaps. Do we need to?
        alerts = []
        for target in targets:
            alert = {}
            alert['end_unixtime'] = target['epoch']
            alert['gracedb_id'] = target['name']
            alert['ra'] = target['ra']
            alert['dec'] = target['dec']
            alert['mag'] = target['mag']
            alert['score'] = target['score']
            alerts.append(alert)

        return HttpResponse(json.dumps(alerts), content_type="application/json")
