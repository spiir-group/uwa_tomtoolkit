import json
import logging
import time

import astropy.units as u
import astropy_healpix as ah
import healpy as hp
import numpy as np
from astropy.time import Time
from ligo.skymap import bayestar, distance, moc
from ligo.skymap.io import events
from scipy.stats import norm
from spiir.io.ligolw import load_ligolw_xmldoc
from tom_targets.models import Target

logger = logging.getLogger(__name__)


def get_skymap(coinc):
    # Makes skymap from coinc file, adds columns used in later functions
    # Returns sorted skymap with invalid values (infinite distance) removed
    skymap = bayestar.localize(events.ligolw.open(coinc, psd_file=coinc)[1])
    skymap['PROB'] = skymap['PROBDENSITY'] * moc.uniq2pixarea(skymap['UNIQ'])
    skymap['ORDER'], skymap['IPIX'] = moc.uniq2nest(skymap['UNIQ'])
    skymap['NSIDE'] = 2 ** np.int32(skymap['ORDER'])
    return np.sort(skymap[~np.isinf(skymap['DISTMU'])], order='PROB')[::-1]


def loc_coinc_check(skymap, coords):
    # Finds confidence interval at given RA, Dec position (coords) on skymap
    # Based off of fast binary search, explained further at:
    # https://emfollow.docs.ligo.org/userguide/tutorial/multiorder_skymaps.html#probability-density-at-a-known-position
    max_order = np.max(skymap['ORDER'])
    idxs = skymap['IPIX'] * ((2 ** np.int64(max_order - skymap['ORDER'])) ** 2)
    sorter = np.argsort(idxs)
    em_ipix = ah.lonlat_to_healpix(
        coords[0] * u.deg, coords[1] * u.deg, 2 ** np.int32(max_order), order='nested'
    )
    cprob = np.cumsum(skymap['PROB'])
    conf_intvl = cprob[
        sorter[np.searchsorted(idxs, em_ipix, side='right', sorter=sorter)] - 1
    ]
    return conf_intvl


def gc_dist(gw_dec, kn_dec, d_ra):
    # Calculates great-circle distance between every GW skymap pixel & the coordinates
    # of the Fink alert
    gc_num = [
        np.cos(kn_dec) * np.sin(d_ra),
        (
            np.cos(gw_dec) * np.sin(kn_dec)
            - np.sin(gw_dec) * np.cos(kn_dec) * np.cos(d_ra)
        ),
    ]
    gc_denom = np.sin(gw_dec) * np.sin(kn_dec) + np.cos(gw_dec) * np.cos(
        kn_dec
    ) * np.cos(d_ra)
    return np.arctan2(np.linalg.norm(gc_num, axis=0), gc_denom)


def spatial_overlap(skymap, coords, fwhm):
    # Computes spatial posterior overlap integral between the two signals
    # GW spatial posterior: skymap
    # Fink alert spatial posterior: Gaussian point-spread function
    pix_coords = np.array(
        ah.healpix_to_lonlat(skymap['IPIX'], skymap['NSIDE'], order='nested')
    )
    ra_diff = pix_coords[0] - np.full_like(pix_coords[0], np.deg2rad(coords[0]))
    fink_dec = np.full_like(pix_coords[1], np.deg2rad(coords[1]))
    total_err = np.add(
        hp.nside2resol(skymap['NSIDE']) / 2, np.deg2rad(fwhm * (60 * 60 * 1.01 / 2.355))
    )
    overlap_s = np.sum(
        skymap['PROB']
        * norm.pdf(gc_dist(pix_coords[1], fink_dec, ra_diff), scale=total_err)
    )
    skymap['P_BIAS'] = overlap_s / np.sum(overlap_s)

    # Biases GW distance in direction of Fink alert as it's convenient to do here
    # A more accurate estimate of GW event distance if pair are genuinely associated
    biased_dist = distance.parameters_to_marginal_moments(
        skymap['P_BIAS'], skymap['DISTMU'], skymap['DISTSIGMA']
    )
    return ((4 * np.pi) ** 2) * overlap_s, biased_dist


def fink_coinc_check(payload, skymap, alert):
    # Checks whether a Fink alert is coincident in space & time with a GW event
    # Returns posterior overlap between the pair if so; None otherwise
    # Posterior overlap represents association significance
    # Uses method found here: https://arxiv.org/pdf/1712.05392.pdf
    # Also returns GW distance biased in direction of Fink alert if applicable

    # I'm assuming we're only checking GW events within 7 days prior to a Fink alert
    # If optical transient began before GW event, pair are not coincident
    if (
        payload['data']['gpstime']
        >= Time(alert['candidate']['jdstarthist'], format='jd').gps
    ):
        overlap_t = (7 * 24 * 60 * 60) / (
            Time(alert['candidate']['jd'], format='jd').gps - payload['data']['gpstime']
        )

        # Spatial coincidence check: is Fink alert within GW skymap 90% credible region?
        coords = [alert['candidate']['ra'], alert['candidate']['dec']]
        conf_intvl = loc_coinc_check(skymap, coords)
        if conf_intvl < 0.9:
            overlap_s, biased_dist = spatial_overlap(
                skymap, coords, alert['candidate']['fwhm']
            )
            association = overlap_t * overlap_s
        else:
            association, biased_dist = None, None
    else:
        association, biased_dist = None, None

    # Posterior overlap of both signals represented by 'association'
    # Higher value means association is more significant, can use to rank signals
    # Tentative estimate: association >1 isn't very notable, >10 is decent, >100 is
    # amazing
    # TODO: Better priors for temporal & spatial overlap (currently uniform)
    # TODO: Statistical analysis; exactly what 'association' values are worth followup?
    return association, biased_dist


def check_if_coincident(fink_alert=None, gw_alert=None, coinc=None):
    if fink_alert is None and gw_alert is None:
        logger.error("Must supply either fink alert of gwalert.")
        return

    if gw_alert is None:
        check = 'gw_alert'
        startswith = "gw-"
    elif fink_alert is None:
        check = 'fink_alert'
        startswith = "fink-"

    # Get all alerts of other within last 5 minutes
    targets = Target.objects.all().filter(
        epoch__gte=time.time() - 60 * 5, name__startswith=startswith
    )

    for target in targets:
        event_id = target.name.replace(startswith, '')
        if check == 'gw_alert':
            with open(f"gw_alerts/{event_id}/payload.json") as f:
                gw_alert = json.load(f)
            coinc = load_ligolw_xmldoc(f"gw_alerts/{event_id}/coinc.xml")
        elif check == 'fink_alert':
            # TODO not sure what fink saves alert as.
            fink_alert = f"fink_alerts/{event_id}/alert.json"

        skymap = get_skymap(coinc)

        association, biased_dist = fink_coinc_check(gw_alert, skymap, fink_alert)

        threshold = 10  # ?
        if association > threshold:
            # TODO how to decide which of gw/fink's
            # epoch/distance/ra/dec etc. to choose?
            target = Target.objects.create(
                name=f"coinc-{gw_alert['graceid']}-{fink_alert['i:candid']}",
                type='SIDEREAL',
                epoch=target.epoch,
                distance=target.distance,
                ra=target.ra,
                dec=target.dec,
                score=association,
            )
            target.save()
