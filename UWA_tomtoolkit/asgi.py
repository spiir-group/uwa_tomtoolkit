"""
ASGI config for UWA_tomtoolkit project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import asyncio
import logging
import os
import threading

from django.core.asgi import get_asgi_application
from spiir.logging import setup_logger

from .fink_consumer import FinkConsumer
from .GWAlert_consumer import GWAlertConsumer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'UWA_tomtoolkit.settings')


async def listen_to_GWAlerts():
    setup_logger('gwalert-consumer', logging.INFO, 'gwalert-consumer.log')
    logger = logging.getLogger('gwalert-consumer')

    # TODO: Is there a better way to handle exceptions.
    # Should this be a seperate process entirely, run through supervisord?
    while True:
        logger.info('Starting GWAlertConsumer.')
        try:
            # `with` context allows consumer to set up and tear down state if required
            with GWAlertConsumer(
                username='luke.davis-9a879216',
                server='kafka://kafka.scimma.org/',
                group='gracedb-playground',
                topics=['test_spiir_allsky', 'test_spiir_earlywarning'],
                credentials='~/.config/hop/auth.toml',
                save_dir='gw_alerts/',
            ) as consumer:
                consumer.subscribe()  # listen to IGWNAlert Kafka topics on loop
        except Exception as e:
            logger.error("Unexpected failure.")
            logger.error(e)


async def listen_to_Fink():
    setup_logger('fink-consumer', logging.INFO, 'fink-consumer.log')
    logger = logging.getLogger('fink-consumer')

    # TODO: Is there a better way to handle exceptions.
    # Should this be a seperate process entirely, run through supervisord?
    while True:
        logger.info('Starting FinkConsumer.')
        try:
            # `with` context allows consumer to set up and tear down state if required
            with FinkConsumer() as consumer:
                consumer.listen_for_alerts()  # listen to IGWNAlert Kafka topics on loop
        except Exception as e:
            logger.error("Unexpected failure.")
            logger.error(e)


LOOP = None


def run_loop():
    global LOOP
    LOOP = asyncio.new_event_loop()
    LOOP.create_task(listen_to_GWAlerts())
    LOOP.create_task(listen_to_Fink())
    LOOP.run_forever()


threading.Thread(target=run_loop).start()

application = get_asgi_application()
