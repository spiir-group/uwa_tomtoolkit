# TODO must get credentials to receive fink livestream data
# https://fink-broker.readthedocs.io/en/latest/services/livestream/
import time
from datetime import datetime

from fink_client.configuration import load_credentials
from fink_client.consumer import AlertConsumer
from tom_targets.models import Target

from .ranking_system import check_if_coincident


def _convert_to_target(alert):
    # Columns explained here: https://fink-portal.org/api/v1/columns
    # TODO is this a good id to pick?
    name = f"fink-{alert['i:candid']}"
    lastdate_datetime = datetime(alert['lastdate'])
    epoch = time.mktime(lastdate_datetime.timetuple())
    # distance =
    # TODO: Calculate distance_err
    ra = alert['i:ra']
    dec = alert['i:dec']
    target = Target.objects.create(
        name=name, type='SIDEREAL', epoch=epoch, ra=ra, dec=dec
    )
    return target


class FinkConsumer:
    def __init__(self):
        # load user configuration
        conf = load_credentials()

        myconfig = {
            "username": conf['username'],
            'bootstrap.servers': conf['servers'],
            'group_id': conf['group_id'],
        }

        if conf['password'] is not None:
            myconfig['password'] = conf['password']

        self.consumer = AlertConsumer(conf['mytopics'], myconfig, schema_path=None)

        self.conf = conf

    def listen_for_alerts(self):
        while True:
            # Save alerts on disk
            topic, alert, key = self.consumer.poll_and_write(
                outdir='fink_alerts', timeout=self.conf['maxtimeout'], overwrite=True
            )

            try:
                target = Target.objects.get(name=f"fink-{alert['i:candid']}")
            except Target.DoesNotExist:
                target = _convert_to_target(alert)
                target.save()

            check_if_coincident(fink_alert=alert)
