"""Module for the IGWNAlert Consumer to process coinc data and convert to \
    LCO's TOM-Toolkit format."""

import logging
import time
from typing import Any, Dict, Optional

import lal
from django.core.files import File
from spiir.io.igwn.alert.consumer import IGWNAlertConsumer
from tom_dataproducts.models import DataProduct
from tom_targets.models import Target

from .ranking_system import check_if_coincident

logger = logging.getLogger('consumer')


def _convert_to_target(payload, coinc):
    # Target columns defined here: https://tom-toolkit.readthedocs.io/en/stable/api/tom_targets/models.html#tom_targets.models.Target
    # score and mag added to EXTRA_FIELDS in settings.py
    data = payload['data']
    name = f"gw-{data['graceid']}"
    # epoch is described in docs as "Julian Years" ?
    epoch = lal.gpstime.gps_to_utc(data['gpstime'])
    distances = [
        sngl['eff_distance'] for sngl in data['extra_attributes']['SingleInspiral']
    ]
    distance = sum(distances) / len(distances)
    # TODO: Calculate distance_err
    ra = coinc['tables']['postcoh']['ra']
    dec = coinc['tables']['postcoh']['dec']
    target = Target.objects.create(
        name=name, type='SIDEREAL', epoch=epoch, distance=distance, ra=ra, dec=dec
    )
    return target


class GWAlertConsumer(IGWNAlertConsumer):
    def process_payload(
        self,
        payload: Dict[str, Any],
        coinc_cache_glob: Optional[str] = None,
    ):
        runtime = time.perf_counter()

        if self.save_dir is None:
            logger.error(f"[{self.id}] save_dir must be specified.")

        event_id = payload['uid']
        event_dir_path = self.save_dir / event_id
        event_dir_path.mkdir(exist_ok=True, parents=True)

        if self.save_payload:
            payload_fp = event_dir_path / "payload.json"
            if payload_fp.is_file():
                self._write_json(payload, payload_fp)
                logger.info(
                    f"[{self.id}] Saving {event_id} payload file to: {payload_fp}."
                )
            else:
                payload_fp = event_dir_path / f"payload_{time.time()}.json"
                self._write_json(payload, payload_fp)
                logger.info(
                    f"[{self.id}] Saving {event_id} updated payload file to: \
                        {payload_fp}."
                )

        coinc = self._load_coinc_from_gid(
            event_id,
            coinc_cache_glob,
            keys=["tables"],
        )

        try:
            target = Target.objects.get(name=event_id)
        except Target.DoesNotExist:
            target = _convert_to_target(payload, coinc)
            target.save()
            # TODO: publish to kafka stream?

        check_if_coincident(gw_alert=payload)

        # Get list of files associated with event_id
        event_files = self.gracedb.files(event_id).json()
        for filename in list(event_files):
            save_dir = event_dir_path / filename
            # Skip file if versioned or already downloaded
            if ',' in filename or save_dir.exists():
                continue
            # Save file to event_id folder in save_dir
            response = self.gracedb.files(event_id, filename)
            with open(save_dir, 'wb') as f:
                f.write(response.read())
            logger.info(f"[{self.id}] Saved to disk: {save_dir}")

            django_file = File(open(save_dir))
            data_product_type = None
            if save_dir.suffix == '.png':
                data_product_type = 'image_file'
            elif save_dir.suffix == '.fits':
                data_product_type = 'fits_file'
            else:
                continue

            data_product = DataProduct.objects.get_or_create(
                target=target, data_product_type=data_product_type, data=django_file
            )
            data_product.save()
            # TODO: publish to kafka stream?

        runtime = time.perf_counter() - runtime
        logger.debug(f"[{self.id}] Alert for {event_id} processed in {runtime:.4f}s.")
