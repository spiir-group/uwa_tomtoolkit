FROM mambaorg/micromamba

COPY environment.yml .

RUN --mount=type=cache,target="~/.mamba/pkgs",sharing=locked \
    --mount=type=cache,target="/home/$MAMBA_USER/.cache" \
    micromamba install -y -n base -f environment.yml

COPY --chown=$MAMBA_USER:$MAMBA_USER . /repo/

WORKDIR /repo

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "127.0.0.1:8000"]
